<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EmailValidityFormRequest;

class UserController extends Controller
{

    /**
     * Post(
     *      path="/user",
     * 
     *      summary="check email validity",
     *      description="Create an event",  		
     *      	RequestBody(
     *              email: "example@email.com    
     *         JsonContent(ref="#/components/schemas/Event"),
     *         
     *     ),
     *      Response(
     *          response=200,
     *          description="successful operation"
     *       )
     *     )
     *
     */
    public function checkEmailValidity(EmailValidityFormRequest $request){
      
        return $this->mailboxlayerCheck($request->email);

    }

    public function mailboxlayerCheck($email){
        $access_key = config('app.mailBox_access_key');  ;
        // set email address
        $email_address = $email;

        // Initialize CURL:
        $ch = curl_init('http://apilayer.net/api/check?access_key='.$access_key.'&email='.$email_address.'');  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Store the data:
        $json = curl_exec($ch);
        curl_close($ch);

        // // Decode JSON response:
        $validationResult = json_decode($json, true);

        return response()->json($validationResult, 200);
        // Access and use your preferred validation result objects
    }
}
